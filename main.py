# -*- coding: utf-8 -*-

import os
import PIL
import pandas as pd
import PIL.Image as PILimage
import requests
from PIL import ImageDraw, ImageFont, ImageEnhance
from PIL.ExifTags import TAGS, GPSTAGS
from csv import writer
from urllib.parse import parse_qs
from PyQt5.QtCore import Qt
from PyQt5.Qt import QIcon
from conf_public import *

class Worker(object):

    def __init__(self, filename, fullpath):
        super(Worker, self).__init__()

        self.filename = filename
        self.fullpath = fullpath
        self.header = ['FileName', 'Lat', 'Lon', 'Date']
        self.img = self.open()

        self.exif_data = self.get_exif_data()

        self.lat = self.get_lat()
        self.lon = self.get_lon()
        self.date = self.get_date_time()

        self.data = [self.filename, self.lat, self.lon, self.date]
        self.csv_writer()

        self.true_orient = self.set_orient()
        self.compress = self.compress()
        self.add_watermark()
        #
        #
        for i in range(0, 3):
            self.add_infobox()
            self.add_mapping(15 + i + 1)
        #
        #
        # self.dataframe()

    @staticmethod
    def get_if_exist(data, key):
        if key in data:
            return data[key]
        return None

    @staticmethod
    def convert_to_degress(value):

        """Helper function to convert the GPS coordinates
        stored in the EXIF to degress in float format"""
        d0 = value[0][0]
        d1 = value[0][1]
        d = float(d0) / float(d1)

        m0 = value[1][0]
        m1 = value[1][1]
        m = float(m0) / float(m1)

        s0 = value[2][0]
        s1 = value[2][1]
        s = float(s0) / float(s1)

        return d + (m / 60.0) + (s / 3600.0)

    def open(self):
        f = PILimage.open(self.fullpath)
        return f

    def get_exif_data(self):
        """Returns a dictionary from the exif data of an PIL Image item. Also
        converts the GPS Tags"""
        exif_data = {}
        info = self.img._getexif()
        if info:
            for tag, value in info.items():
                decoded = TAGS.get(tag, tag)
                if decoded == "GPSInfo":
                    gps_data = {}
                    for t in value:
                        sub_decoded = GPSTAGS.get(t, t)
                        gps_data[sub_decoded] = value[t]

                    exif_data[decoded] = gps_data
                else:
                    exif_data[decoded] = value
        # self.exif_data = exif_data
        return exif_data

    def get_lat(self):
        """Returns the latitude and longitude, if available, from the provided
        exif_data (obtained through get_exif_data above)"""
        if 'GPSInfo' in self.exif_data:
            gps_info = self.exif_data["GPSInfo"]
            gps_latitude = self.get_if_exist(gps_info, "GPSLatitude")
            gps_latitude_ref = self.get_if_exist(gps_info, 'GPSLatitudeRef')
            if gps_latitude and gps_latitude_ref:
                lat = self.convert_to_degress(gps_latitude)
                if gps_latitude_ref != "N":
                    lat = 0 - lat
                lat = float(f"{lat:.{5}f}")
                return lat
        else:
            return None

    def get_lon(self):
        """Returns the latitude and longitude, if available, from the provided
        exif_data (obtained through get_exif_data above)"""
        if 'GPSInfo' in self.exif_data:
            gps_info = self.exif_data["GPSInfo"]
            gps_longitude = self.get_if_exist(gps_info, 'GPSLongitude')
            gps_longitude_ref = self.get_if_exist(gps_info, 'GPSLongitudeRef')
            if gps_longitude and gps_longitude_ref:
                lon = self.convert_to_degress(gps_longitude)
                if gps_longitude_ref != "E":
                    lon = 0 - lon
                lon = float(f"{lon:.{5}f}")
                return lon
        else:
            return None

    def get_date_time(self):
        if 'DateTime' in self.exif_data:
            date_and_time = self.exif_data['DateTime']
            return date_and_time
        else:
            return None

    def set_orient(self):
        try:
            if 'Orientation' in self.exif_data:
                orientation = self.exif_data['Orientation']

                if orientation == 6:
                    rty = (self.img.width - self.img.height) / 2
                    img = self.img
                    img = img.rotate(-90)

                    img = img.crop(box=(rty, 0, img.width - rty,
                                        img.height))
                    self.img = img

                elif orientation == 3:
                    img = self.img
                    img = img.rotate(180)
                    self.img = img

                else:
                    pass
        except Exception as e:
            print(e)

    def compress(self):
        basewidth, baseheight = self.img.size
        # compress = 100 - COMPERSSION_LVL
        w = int(basewidth / 100 * COMPERSSION_LVL)
        h = int(baseheight / 100 * COMPERSSION_LVL)
        if w > MAX_WIDTH:
            koef = w / MAX_WIDTH
            w = MAX_WIDTH
            h = int(h / koef)

        img = self.img
        img = img.resize((w, h), PIL.Image.ANTIALIAS)
        self.img = img

    def add_watermark(self):
        img = self.img
        position = self.calc_pos()
        wm = PILimage.open(watermark_path + watermark_name).convert('L').point(
            lambda x: min(x, OPACITY))

        wm = wm.resize((self.w_w, self.w_w),
                       PIL.Image.ANTIALIAS)

        bg_of_wm = PILimage.new('RGBA',
                                wm.size,
                                WMark_RGBA_LVL)

        bg_of_wm.paste(wm, wm)
        img.paste(wm, position, bg_of_wm)
        self.img = img
        # return img

    def add_infobox(self):
        img = self.img
        infobox = PILimage.new('RGBA',
                               (img.width,
                                int(img.height *
                                    (infobox_size_lvl / 100) + PADDING)),
                               color=(255, 255, 255, 255))
        self.infobox = infobox
        w, h = (img.width, img.height + infobox.height)
        img_with_infobox = PILimage.new('RGB', (w, h))
        img_with_infobox.paste(img, (0, 0))
        # img_with_infobox.show()
        self.img = img_with_infobox

    def add_mapping(self, zoom_of_map):
        img = self.img
        infobox = self.infobox
        r = requests.get(
            url + '/' + style + '/' + str(self.lon) + ',' + str(
                self.lat) + ',' + str(zoom_of_map) +
            ',0,0/' + str(infobox.width) + 'x' + str(infobox.height),
            params=params)
        map_content = r.content
        with open('_map.jpg', 'wb') as out:
            out.write(map_content)

        map_image = PILimage.open('_map.jpg')
        position = (  # (img.width - map_image.width - PADDING),
            (0),
            (img.height - map_image.height + PADDING))

        img.paste(map_image, position)
        self.img = img

    def calc_pos(self):
        img = self.img
        self.w_w = int(float((watermark_size / 100) * img.size[0]))

        # int(float((watermark_size / 100) * img.size[1]))

        # *###############*
        # adding to img #
        # The box argument is either a 2-tuple giving the upper left corner,
        # a 4-tuple defining the left, upper, right, and lower pixel coordinate,
        # or None (same as (0, 0)). If a 4-tuple is given,
        # the size of the pasted image must match the size of the region.
        position = ((img.width - self.w_w),
                    (img.height - self.w_w - PADDING))

        self.pos = position
        return position

    def csv_writer(self):
        if not os.path.exists(input_csv):
            with open('data.csv', "w") as csv_file:
                w = writer(csv_file)
                w.writerow(self.header)
                w.writerow(self.data)
        else:
            with open('data.csv', 'a') as csvfile:
                w = writer(csvfile, delimiter=',')
                w.writerow(self.data)

    def dataframe(self):
        df = pd.read_csv(input_csv)

        # df_reindexed = df.reindex(columns=df_head)
        # df[df_head].head(5)
        # df.rename(columns=df_head, inplace=True)
        print(df)

        # df.drop_duplicates(subset=None, inplace=True)
        df.to_csv(output_csv)

    def save(self, output_filename):
        img = self.img
        img.save(output_filename)


# class Example(QWidget):
#     def __init__(self):
#         super().__init__()
#         self.initUI()
#
#     def initUI(self):
#         # self.textEdit = QTextEdit()
#         # self.setCentralWidget(self.textEdit)
#         # self.statusBar()
#
#         # self.label = QLabel(self)
#         # self.label.setPixmap(QPixmap('if_folder-open_118942 (1).png'))
#         # self.label.setGeometry(100, 50, 80, 20)
#
#         self.openButton = QPushButton(self)
#         self.openButton.setGeometry(50, 50, 80, 70)
#         self.openButton.setIcon(QIcon('if_folder-open_118942 (1).png'))
#         self.openButton.clicked.connect(self.showDialog)
#
#         self.setGeometry(200, 200, 200, 200)
#         self.setWindowTitle('File dialog')
#         self.show()
#
#     def showDialog(self):
#         fname = QFileDialog.getOpenFileName(self, 'Open file', path)
#         print(fname)
#         return fname


if __name__ == '__main__':
    try:
        for root, folders, filenames in os.walk(path):
            for filename in filenames:

                if filename.endswith('.jpg'):
                    print(count)
                    print(f'Filename:----{filename}')
                    fullpath = os.path.join(root, filename)
                    # fn, fext = os.path.splitext(filename)

                    # app = QApplication(sys.argv)
                    # ex = Example()
                    # sys.exit(app.exec_())

                    image = Worker(filename, fullpath)

                    if count == 0:
                        output_filename = (
                            f'{path}{image.lat}, {image.lon}{count}.JPG')
                        image.save(output_filename)
                        print(f'Saved as:-------{image.lat}, {image.lon}' + '\n')
                        count += 1

                    else:
                        output_filename = (
                            f'{path}{image.lat}, {image.lon}{count}.JPG')
                        image.save(output_filename)
                        print(f'Saved as:-------{image.lat}, {image.lon}' + '\n')
                        count += 1


    except Exception as e:
        exit(e)
